# Lab 06: Infrastructure-as-Code and Configuration Management - Terraform, Ansible and GitLab
Authors : Cédric Centeno, Yann Merk
Group B

Small web infrastructure managed by Terraform and Ansible

## Task 2

### 1 - Explain the usage of each provided file and its contents by directly adding comments in the file as needed (we must ensure that you understood what you have done).

*In the file `variables.tf` fill the missing documentation parts and link to the online documentation. Copy the modified files to the report.*
# TODO  link to documentation ?

Here are the commented files :
#### backend.tf
```terraform
# Define where the Terraform config is stored.  Here, in a local file
terraform {
  backend "local" {
  }
}
```

#### main.tf
```terraform
# Sets up a basis Google Compute instance, and enable network access

provider "google" {
  project     = var.gcp_project_id
  region      = "europe-west6-a"
  credentials = file("${var.gcp_service_account_key_file_path}")
}

# Provision a Google compute f1-micro instance, connected to the default network
resource "google_compute_instance" "default" {
  name         = var.gce_instance_name
  machine_type = "f1-micro"
  zone         = "europe-west6-a"

  metadata = {
    # Apply the given ssh public key, to allow access to the bearer of the private key
    ssh-keys = "${var.gce_instance_user}:${file("${var.gce_ssh_pub_key_file_path}")}"
  }

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  network_interface {
    network = "default"

    access_config {
      # Include this section to give the VM an external IP address
    }
  }
}

# Allow ssh (port 22) access through Google compute's Firewall, on the default network
resource "google_compute_firewall" "ssh" {
  name          = "allow-ssh"
  network       = "default"
  source_ranges = ["0.0.0.0/0"]
  allow {
    ports    = ["22"]
    protocol = "tcp"
  }
}

# Allow http (port 80) access through Google compute's Firewall, on the default network
resource "google_compute_firewall" "http" {
  name          = "allow-http"
  network       = "default"
  source_ranges = ["0.0.0.0/0"]
  allow {
    ports    = ["80"]
    protocol = "tcp"
  }
}
```

#### outputs.tf
```terraform
# Outputs the instance's external IP address
output "gce_instance_ip" {
  value = google_compute_instance.default.network_interface.0.access_config.0.nat_ip
}
```

#### terraform.tfvars
```terraform
gcp_project_id = "wide-silo-387815"
gcp_service_account_key_file_path = "../credentials/labgce-service-account-key.json"
gce_instance_name = "instancy-mc-instanceface"
gce_instance_user = "pistou"
gce_ssh_pub_key_file_path = "../credentials/labgce-ssh-key.pub"
```

#### variables.tf
```terraform
# Define all the possible variable for the provisionning
# The variables are set in the `terraform.tfvars` file
variable "gcp_project_id" {
  description = "The Google Project id, found on the console"
  type        = string
  nullable    = false
}

variable "gcp_service_account_key_file_path" {
  description = "The Google project access credentials, to update the infrastructure from Terraform"
  type        = string
  nullable    = false
}

variable "gce_instance_name" {
  description = "The name given to the GCE Instance created by Terraform"
  type        = string
  nullable    = false
}

variable "gce_instance_user" {
  description = "The user account created on the GCE Instance, paired to the `gce_ssh_pub_key_file_path` public key"
  type        = string
  nullable    = false
}

variable "gce_ssh_pub_key_file_path" {
  description = "The public key paired to `gce_instance_user`, for ssh access to the gce_instance"
  type        = string
  nullable    = false
}
```


### 2 - Explain what the files created by Terraform are used for.
They are created to store the required plugins to interact with google cloud, and more 
importantly to store the latest state set by terraform.

### 3 - Where is the Terraform state saved? Imagine you are working in a team and the other team members want to use Terraform, too, to manage the cloud infrastructure. Do you see any problems with this? Explain.
In local, [terraform/terraform.tfstate](terraform/terraform.tfstate).  A backup of the previous state is also kept in [terraform/terraform.tfstate.backup](terraform/terraform.tfstate.backup)

It can lead to huge problem, as the other members might try to update the infrastructure without knowing it's current state
set by terraform, which could end up for example with duplicate instances, or unwanted deletions.

### 4 - What happens if you reapply the configuration (1) without changing main.tf (2) with a change in main.tf? Do you see any changes in Terraform's output? Why? Can you think of exemples where Terraform needs to delete parts of the infrastructure to be able to reconfigure it?
#### (1)
```
╷
│ Error: Saved plan is stale
│
│ The given plan file can no longer be applied because the state was changed by another operation after the plan was created.
```
The plan cannot be applied by just running the `apply` command once more

and trying to rerun the `plan` command will result in this :
```
google_compute_firewall.ssh: Refreshing state... [id=projects/wide-silo-387815/global/firewalls/allow-ssh]
google_compute_instance.default: Refreshing state... [id=projects/wide-silo-387815/zones/europe-west6-a/instances/instancy-mc-instanceface]
google_compute_firewall.http: Refreshing state... [id=projects/wide-silo-387815/global/firewalls/allow-http]

No changes. Your infrastructure matches the configuration.

Terraform has compared your real infrastructure against your configuration and found no differences, so no changes are needed.
```

An `apply` run after will *complete* like so :
```
Apply complete! Resources: 0 added, 0 changed, 0 destroyed.

Outputs:

gce_instance_ip = "34.65.221.6"
```

#### (2)
Trying to change the instance name will result in Terraform deleting the current instance, and creating a new one with the new name

Here's what a `plan` returns :
```
google_compute_firewall.http: Refreshing state... [id=projects/wide-silo-387815/global/firewalls/allow-http]
google_compute_instance.default: Refreshing state... [id=projects/wide-silo-387815/zones/europe-west6-a/instances/instancy-mc-instanceface]
google_compute_firewall.ssh: Refreshing state... [id=projects/wide-silo-387815/global/firewalls/allow-ssh]

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
-/+ destroy and then create replacement

Terraform will perform the following actions:

  # google_compute_instance.default must be replaced
-/+ resource "google_compute_instance" "default" {
      ~ cpu_platform         = "Intel Skylake" -> (known after apply)
      ~ current_status       = "RUNNING" -> (known after apply)
      - enable_display       = false -> null
      ~ guest_accelerator    = [] -> (known after apply)
      ~ id                   = "projects/wide-silo-387815/zones/europe-west6-a/instances/instancy-mc-instanceface" -> (known after apply)
      ~ instance_id          = "4284219744772354384" -> (known after apply)
      ~ label_fingerprint    = "42WmSpB8rSM=" -> (known after apply)
      - labels               = {} -> null
      ~ metadata_fingerprint = "m-v8kjCLnqQ=" -> (known after apply)
      + min_cpu_platform     = (known after apply)
      ~ name                 = "instancy-mc-instanceface" -> "instancy-mc-instanceface2" # forces replacement
      ~ project              = "wide-silo-387815" -> (known after apply)
      - resource_policies    = [] -> null
      ~ self_link            = "https://www.googleapis.com/compute/v1/projects/wide-silo-387815/zones/europe-west6-a/instances/instancy-mc-instanceface" -> (known after apply)
      - tags                 = [] -> null
      ~ tags_fingerprint     = "42WmSpB8rSM=" -> (known after apply)
        # (5 unchanged attributes hidden)

      ~ boot_disk {
          ~ device_name                = "persistent-disk-0" -> (known after apply)
          + disk_encryption_key_sha256 = (known after apply)
          + kms_key_self_link          = (known after apply)
          ~ source                     = "https://www.googleapis.com/compute/v1/projects/wide-silo-387815/zones/europe-west6-a/disks/instancy-mc-instanceface" -> (known after apply)
            # (2 unchanged attributes hidden)

          ~ initialize_params {
              ~ image  = "https://www.googleapis.com/compute/v1/projects/debian-cloud/global/images/debian-11-bullseye-v20230509" -> "debian-cloud/debian-11"
              ~ labels = {} -> (known after apply)
              ~ size   = 10 -> (known after apply)
              ~ type   = "pd-standard" -> (known after apply)
            }
        }

      ~ network_interface {
          + ipv6_access_type   = (known after apply)
          ~ name               = "nic0" -> (known after apply)
          ~ network            = "https://www.googleapis.com/compute/v1/projects/wide-silo-387815/global/networks/default" -> "default"
          ~ network_ip         = "10.172.0.2" -> (known after apply)
          - queue_count        = 0 -> null
          ~ stack_type         = "IPV4_ONLY" -> (known after apply)
          ~ subnetwork         = "https://www.googleapis.com/compute/v1/projects/wide-silo-387815/regions/europe-west6/subnetworks/default" -> (known after apply)
          ~ subnetwork_project = "wide-silo-387815" -> (known after apply)

          ~ access_config {
              ~ nat_ip       = "34.65.221.6" -> (known after apply)
              ~ network_tier = "PREMIUM" -> (known after apply)
            }
        }

      - scheduling {
          - automatic_restart   = true -> null
          - min_node_cpus       = 0 -> null
          - on_host_maintenance = "MIGRATE" -> null
          - preemptible         = false -> null
          - provisioning_model  = "STANDARD" -> null
        }

      - shielded_instance_config {
          - enable_integrity_monitoring = true -> null
          - enable_secure_boot          = false -> null
          - enable_vtpm                 = true -> null
        }
    }

Plan: 1 to add, 0 to change, 1 to destroy.

Changes to Outputs:
  ~ gce_instance_ip = "34.65.221.6" -> (known after apply)

─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────── 

Saved the plan to: .terraform/plan.cache

To perform exactly these actions, run the following command to apply:
    terraform apply ".terraform/plan.cache"
```

It's probably because Google Cloud Engine's API doesn't allow to rename an instance, so Terraform will attempt to reach
the desired state with a solution as extreme as deleting the instance.

It's likely any operation for which the API doesn't support a "change" would result in a similar result

As a counter example, changing a port in a firewall rule is supported for an "update in place" (here changing the port 22 to 21) :
```
Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  ~ update in-place

Terraform will perform the following actions:

  # google_compute_firewall.ssh will be updated in-place
  ~ resource "google_compute_firewall" "ssh" {
        id                      = "projects/wide-silo-387815/global/firewalls/allow-ssh"
        name                    = "allow-ssh"
        # (13 unchanged attributes hidden)

      - allow {
          - ports    = [
              - "22",
            ] -> null
          - protocol = "tcp" -> null
        }
      + allow {
          + ports    = [
              + "21",
            ]
          + protocol = "tcp"
        }
    }

Plan: 0 to add, 1 to change, 0 to destroy.
```

### 5 - Explain what you would need to do to manage multiple instances.
Add a new `google_compute_instance` ressource in main.tf, with a different name

### 6 - Screenshot of the Google Cloud instance
Here's a screenshot of the newly created instance, on Google Cloud's console
![Console_GCE_Task2.png](images/Console_GCE_Task2.png)

## Task 4
### Difficulties
Ansible couldn't run under WSL as is, we had to copy the WSL private key to a local WSL folder, `chown 700` it, and then
we could finally use Ansible without an SSH error.

... That was until we followed the steps to create an `ansibile.cfg` file. Ansible really doesn't like having it under WSL.
https://docs.ansible.com/ansible/devel/reference_appendices/config.html#cfg-in-world-writable-dir

We had to follow the [linked documentation on Microsoft's blog](https://devblogs.microsoft.com/commandline/chmod-chown-wsl-improvements/)
to temporary go around this issue
```bash
cd /
sudo umount /mnt/c
sudo mount -t drvfs C: /mnt/c -o metadata

cd <path to project>
sudo chown <user>:<user> .
chmod 700 .
```
Which fixed the issue

### What happens if the infrastructure is deleted and then recreated with Terraform? What needs to be updated to access the infrastructure again?
The [hosts](ansible/hosts) file needs to be updated with the new instance's IP address (-> terraform's output), and... that's it.

## Task 5
We've commented [nginx.conf](ansible/playbooks/files/nginx.conf), [web.yml](ansible/playbooks/web.yml) and [index.html.j2](ansible/playbooks/templates/index.html.j2)
to explain their content

Here's the content of our [hosts](ansible/hosts) file:
```
[webservers]
gce_instance ansible_ssh_host=34.65.221.6
```

## Task 6
Here's our [playbook](ansible/playbooks/web.yml)'s content after adding the handler
```yaml
- name: Configure webserver with nginx
  hosts: webservers # The group on which to apply those modifications
  become: True # Requires elevated privileges, see https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_privilege_escalation.html
  tasks:
      # Installs nginx from apt (~ apt install -y nginx), https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_module.html#ansible-collections-ansible-builtin-apt-module
    - name: install nginx
      apt: name=nginx update_cache=yes
      # Copy the provided config file to the proper location, https://docs.ansible.com/ansible/latest/collections/ansible/builtin/copy_module.html
    - name: copy nginx config file
      copy: src=files/nginx.conf dest=/etc/nginx/sites-available/default
      notify:
        - restart nginx # force an nginx restart, to apply the new configuration
      # Creates a symlink to the new configuration, to apply it to nginx, https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html
    - name: enable configuration
      file: >
        dest=/etc/nginx/sites-enabled/default
        src=/etc/nginx/sites-available/default
        state=link
      notify:
          # Done twice, in case the configuration file was set but the symlink didn't exist (will only restart once even if both tasks notify)
        - restart nginx
      # Copy the provided html file, and preprocess it to replace {{ tags }} with the appropriate content
      # https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html
    - name: copy index.html
      template: src=templates/index.html.j2 dest=/usr/share/nginx/html/index.html mode=0644
  # tasks that only run if specific events are triggered (notify)
  # https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_handlers.html#handlers
  handlers:
      # Restart the nginx service
      # https://docs.ansible.com/ansible/latest/collections/ansible/builtin/service_module.html
    - name: restart nginx
      service: name=nginx state=restarted
```

## Task 7
### 1 - Return to the output of running the web.yml playbook the first time. There is one additional task that was not in the playbook. Which one?
`Gathering Facts`, which is where Ansible gathers some basics info about the hosts in the `hosts` file. Those infos
will be used by Ansible to know what kind of ssh commands it will be able to send to each host to check / change them.

### 2 - Among the tasks that are in the playbook there is one task that Ansible marked as ok. Which one? Do you have a possible explanation?
`enable configuration`, as nginx probably already did the symlink to that folder as part of it's initial installation.

### 3 - Re-run the web.yml playbook a second time. In principle nothing should have changed. Compare Ansible's output with the first run. Which tasks are marked as changed?
None. Before task 6, the `restart nginx` task would be marked continuously as changed, since it changed the system in a way.
But after task 6 all tasks are marked as OK, since no changes needs to be made, and no restart either.

### 4 - SSH into the managed server. Modify the NGINX configuration file /etc/nginx/sites-available/default. Re-run the playbook. What does Ansible do to the file and what does it show in its output?
It copies back our configuration file, which deletes the modifications made by ssh. The task is obviously marked as "changed",
plus it also called the `restart nginx` handler, per our configuration.

### 5 - Do something more drastic like completely removing the homepage and repeat the previous question.
It only marked the `copy index.html` task as changed, all the others are marked as ok, as is expected.

### 6 - What is the differences between Terraform and Ansible? Can they both achieve the same goal?
Terraform is used to programmatically deploy infrastructures, including load balancers, virtual machines, firewalls, ...\
It works by connecting to an existing API, provided by a cloud provider.\
It is a Cloud Provisioning Tool.

Ansible on the other end is made to run commands and set configs on the machines themselves, using SSH. It doesn't require any
API or setup on the machines appart from an SSH access.\
It is a Configuration Management Tool.

They cannot do the job of the other, they are completely different tools. The only trait they share is that they use the principle
of Desired State Configuration, meaning they only modify what they need to do to achieve the desired infrastructure/machine state

### 7 - List the advantages and disadvantages of managing your infrastructure with Terraform/Ansible vs. manually managing your infrastructure. In which cases is one or the other solution more suitable?
#### Advantages
- Easy to use
- Can manage a huge amount of components
- Easy to share, for multiple sysadmins
- Can be very easily documented and organized since it uses a small amount of config files
- Can be put in a version control system, to have a way to revert to an old configuration / infrastructure

#### Disadvantages
- All modifications of ressources managed by those tools NEED to be made using those tools, or they will be overridden by the next run. Which might make small fixes longer to implement
- (for Terraform) Requires to use a cloud. ...which might make it unfit for a small in-house deployment, if an internal cloud service isn't setup.
- (for Terraform) If the provider's API doesn't support a type of change, the proposed solution might be to NUKE a system and recreate a new one with the change applied. Every plan must be reviewed carefully.
- (for Ansible) Requires remote access to the 


#### Better fit
For a small on-premise setup, Terraform might not be the preferred way to go. Otherwise, as soon as cloud providers are used
it seems like a very good way to manage any infrastructure.

Ansible doesn't have that downside, and seems to be fit for nearly every kind of deployment, especially as soon as multiple
copies of a server are needed.

### 8 - Small case study
*Suppose you now have a web server in production that you have configured using Ansible. You are working in the IT department
of a company and some of your system administrator colleagues who don't use Ansible have logged manually into some
of the servers to fix certain things. You don't know what they did exactly. What do you need to do to bring all the
server again to the initial state? We'll exclude drastic changes by your colleagues for this question.*

Simply run the Playbook again. As long as the changes the colleagues have made aren't on files not covered by the playbook,
they will be overwritten by our initial config files.
