# Define all the possible variable for the provisionning
# The variables are set in the `terraform.tfvars` file
variable "gcp_project_id" {
  description = "The Google Project id, found on the console"
  type        = string
  nullable    = false
}

variable "gcp_service_account_key_file_path" {
  description = "The Google project access credentials, to update the infrastructure from Terraform"
  type        = string
  nullable    = false
}

variable "gce_instance_name" {
  description = "The name given to the GCE Instance created by Terraform"
  type        = string
  nullable    = false
}

variable "gce_instance_user" {
  description = "The user account created on the GCE Instance, paired to the `gce_ssh_pub_key_file_path` public key"
  type        = string
  nullable    = false
}

variable "gce_ssh_pub_key_file_path" {
  description = "The public key paired to `gce_instance_user`, for ssh access to the gce_instance"
  type        = string
  nullable    = false
}
